vuzix blade

- DoF/FoV
- Connect to osx
- Develop app
- Always on
- disable controls

# Communication tool for qlab to Android

Apps register to Tool in array

QLab sends to Tool which sends to all devices in array

# Installation

- `npm install`

# To run

- `npm start`
- copy the host ip and insert into qlab and android app
- The Android apps will register using this IP
- Qlab will send each message to the tool which sends them to all the devices.

Android -> Tool poort 1336

QLab -> Tool -> Android poort 1337

# Install from repo

- `git clone https://gitlab.com/meint/triater-subserver.git`
- `cd triater-subserver`
- `npm i -g .`

// npm install --save "git@gitlab.com:meint/triater-subserver.git"

Toggle menu on emulator: Command + M
or KEYCODE_DPAD_CENTER
