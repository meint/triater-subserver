#!/usr/bin/env node

var osc = require("osc");
var ip = require("ip");

/****************
 * OSC Over UDP *
 ****************/
const clientIpList = [];
var getIPAddresses = () => {
  var os = require("os"),
    interfaces = os.networkInterfaces(),
    ipAddresses = [];

  for (var deviceName in interfaces) {
    var addresses = interfaces[deviceName];
    for (var i = 0; i < addresses.length; i++) {
      var addressInfo = addresses[i];
      if (addressInfo.family === "IPv4" && !addressInfo.internal) {
        ipAddresses.push(addressInfo.address);
      }
    }
  }

  return ipAddresses;
};

const udpPort = new osc.UDPPort({
  localAddress: ip.address(),
  localPort: 1336
});

udpPort.on("ready", () => {
  const ipAddresses = getIPAddresses();

  console.log("Listening for OSC over UDP.");
  ipAddresses.forEach(address => {
    console.log(" Host:", address + ", Port:", udpPort.options.localPort);
  });
});

udpPort.on("message", oscMessage => {
  // console.log(oscMessage);
  const { address, args } = oscMessage;

  switch (address) {
    case "/register_client":
      const ipIndex = 2;
      const ip = args[ipIndex];
      console.log("registring client", ip);

      if (!clientIpList.includes(ip)) clientIpList.push(ip);

      break;

    case "/send_to_clients":
      const clientPort = 1337;

      const message = encodeURIComponent(args);
      clientIpList.forEach(clientIp => {
        udpPort.send(
          {
            address,
            args: message
          },
          clientIp,
          clientPort
        );
      });
      console.log("sending to clients", clientIpList, message);

      break;

    default:
      console.log("not implemented path: ", address);
      break;
  }
});

udpPort.on("error", err => {
  console.log(err);
});

udpPort.open();
